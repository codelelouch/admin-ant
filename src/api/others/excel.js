/**菜单管理 */
import http from '../../utils/request';

export const excel = {
    getExcel: (params) => {
        return http.get('test', params)
    },

    excelDownload: () => {
        return http.download('test/template', {}, '导入模板.xlsx')
    },
    excelUpload: (data) => {
        return http.upload('test/import', data)
        
    },

    excelExport: () => {
        return http.export('test/export')
    }
}