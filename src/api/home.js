import http from '../utils/request';

export const home = {
    // 上传
    fileUpload: (data) => {
        return http.upload('file/upload', data);
    },
    // 首页
    login: (data) => {
        return http.post('login', data);
    },
    getDicts: (params) => {
        return http.get('dict/trim', params);
    },
    originalGet: () => {
        return http.originalGet('https://api.github.com/users/wuyouzhuguli/repos');
    },
    getInfo: (params) => {
        return http.get(`index/${params}`);
    }
};
