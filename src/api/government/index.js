// 政务服务
import http from '../../utils/request'

export const government = {
  change: (params) => {
    return http.get('/government/change', params)
  },
  del: (data) => {
    return http.post('/government/delete', data)
  },
  info: (params) => {
    return http.get('/government/info', params)
  },
  list: (params) => {
    return http.get('/government/page/list', params)
  },
  save: (data) => {
    return http.post('/government/save', data)
  }
}
