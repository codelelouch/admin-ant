/**菜单管理 */
import http from '../../utils/request';

export const tomcatinfo = {
    created: () => {
        return http.get('actuator/metrics/tomcat.sessions.created')
    },
    expired: () => {
        return http.get('actuator/metrics/tomcat.sessions.expired')
    },
    current: () => {
         return http.get('actuator/metrics/tomcat.sessions.active.current')
    },
    max: () => {
        return http.get('actuator/metrics/tomcat.sessions.active.max')
    },
    rejected: () => {
        return http.get('actuator/metrics/tomcat.sessions.rejected')
    },
    sent: () => {
        return http.get('actuator/metrics/tomcat.global.sent')
    },
    requestMax: () => {
        return http.get('actuator/metrics/tomcat.global.request.max')
    },
    request: () => {
        return http.get('actuator/metrics/tomcat.global.request')
    },
    thredsCurrent: () => {
        return http.get('actuator/metrics/tomcat.threads.current')
    },
    thredsConfig: () => {
        return http.get('actuator/metrics/tomcat.threads.config.max')
    }
}