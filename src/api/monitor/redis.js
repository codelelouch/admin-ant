/**菜单管理 */
import http from '../../utils/request';

export const redis = {
    redis: (params) => {
        return http.get('redis/info', params)
    },
    keysSize: (params) => {
        return http.get('redis/keysSize', params)
    },
    memoryInfo: (params) => {
        return http.get('redis/memoryInfo', params)
    },
    
}