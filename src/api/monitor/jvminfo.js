/**菜单管理 */
import http from '../../utils/request';

export const jvminfo = {
    max: () => {
        return http.get('actuator/metrics/jvm.memory.max')
    },
    committed: () => {
        return http.get('actuator/metrics/jvm.memory.committed')
    },
    used: () => {
        return http.get('actuator/metrics/jvm.memory.used')
    },
    bufferUsed: () => {
        return http.get('actuator/metrics/jvm.buffer.memory.used')
    },
    count: () => {
        return http.get('actuator/metrics/jvm.buffer.count')
    },
    daemon: () => {
        return http.get('actuator/metrics/jvm.threads.daemon')
    },
    live: () => {
        return http.get('actuator/metrics/jvm.threads.live')
    },
    peak: () => {
        return http.get('actuator/metrics/jvm.threads.peak')
    },
    loaded: () => {
        return http.get('actuator/metrics/jvm.classes.loaded')
    },
    unloaded: () => {
        return http.get('actuator/metrics/jvm.classes.unloaded')
    },
    allocated: () => {
        return http.get('actuator/metrics/jvm.gc.memory.allocated')
    },
    promoted : () => {
        return http.get('actuator/metrics/jvm.gc.memory.promoted')
    },
    maxSize: () => {
        return http.get('actuator/metrics/jvm.gc.max.data.size')
    },
    liveSize: () => {
        return http.get('actuator/metrics/jvm.gc.live.data.size')
    },
    pause: () =>{
        return http.get('actuator/metrics/jvm.gc.pause')

    }
    
}