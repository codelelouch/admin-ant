/**菜单管理 */
import http from '../../utils/request';

export const requestlog = {
    requestlog: (params) => {
        return http.get('log', params)
    },

    requestlogExcel: (params) => {
        return http.export('log/excel', params)
    }
}