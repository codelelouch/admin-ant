/**菜单管理 */
import http from '../../utils/request';

export const systeminfo = {
    count: () => {
        return http.get('actuator/metrics/system.cpu.count')
    },
    cpu: () => {
        return http.get('actuator/metrics/system.cpu.usage')
    },
    uptime: () => {
        return http.get('actuator/metrics/process.uptime')
    },
    time: () => {
        return http.get('actuator/metrics/process.start.time')
    },
    usage: () => {
        return http.get('actuator/metrics/process.cpu.usage')
    }
}