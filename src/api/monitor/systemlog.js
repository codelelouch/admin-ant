/**菜单管理 */
import http from '../../utils/request';

export const systemlog = {
    systemlog: (params) => {
        return http.get('log', params)
    },
    deptAdd: (data) => {
        return http.post('dept', data)
    },

    deptEdit: (params) => {
        return http.put(`dept`, params)
    },

    systemlogExcel: (params) => {
        return http.export('log/excel', params)
    }
}