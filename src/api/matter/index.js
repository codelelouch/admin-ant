import http from '../../utils/request';

export const matter = {
    del: (data) => {
        return http.post('/matter/delete', data);
    },
    info: (params) => {
        return http.get('/matter/info', params);
    },
    list: (params) => {
        return http.get('/matter/page/list', params);
    },
    save: (data) => {
        return http.post('/matter/save', data);
    },
    qrcode: (data)=>{
        return http.turnImg('/matter/img', data);
    }
};
