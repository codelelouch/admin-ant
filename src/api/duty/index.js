// 值班管理
import http from '../../utils/request'

export const duty = {
  change: (params) => {
    return http.get('/work/change', params)
  },
  del: (data) => {
    return http.post('/work/delete', data)
  },
  info: (params) => {
    return http.get('/work/info', params)
  },
  list: (params) => {
    return http.get('/work/page/list', params)
  },
  save: (data) => {
    return http.post('/work/save', data)
  }
}
