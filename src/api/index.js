import { home } from './home';
import { user } from './system/user';
import { role } from './system/role';
import { menu } from './system/menu';
import { dept } from './system/dept';
import { dict } from './system/dict';
import { weather } from './weather';
import { online } from './monitor/online';
import { systemlog } from './monitor/systemlog';
import { redis } from './monitor/redis';
import { httptrace } from './monitor/httptrace';
import { jvminfo } from './monitor/jvminfo';
import { systeminfo } from './monitor/systeminfo';
import { tomcatinfo } from './monitor/tomcatinfo';
import { requestlog } from './monitor/requestlog';
import { excel } from './others/excel';
import { recommend } from './recommend/index';
import { notice } from './notice/index';
import { matter } from './matter/index';
import { banner } from './banner/index';
import { duty } from './duty/index';
import { government } from './government/index';

const api = {
    ...home,
    ...user,
    ...role,
    ...menu,
    ...dept,
    ...dict,
    ...weather,
    ...online,
    ...systemlog,
    ...redis,
    ...httptrace,
    ...jvminfo,
    ...systeminfo,
    ...tomcatinfo,
    ...requestlog,
    ...excel,
    recommend,
    matter,
    duty,
    government,
    banner,
    notice
};

export default api;
