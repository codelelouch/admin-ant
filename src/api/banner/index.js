import http from '../../utils/request';

export const banner = {
    del: (params) => {
        return http.get('/banner/delete', params);
    },
    list: (params) => {
        return http.get('/banner/page/list', params);
    },
    save: (data) => {
        return http.post('/banner/save', data);
    }
};
