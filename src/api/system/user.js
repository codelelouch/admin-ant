/**系统管理 */
import http from '../../utils/request';

export const user = {
    userList: (params) => {
        return http.get('user', params)
    },
    userAdd: (data) => {
        return http.post('user', data)
    },
    userCheck: (params) => {
        return http.get(`user/check/${params}`)
    },
    userRole: (params) => {
        return http.get('role', params)
    },
    userDept: (params) => {
        return http.get('dept', params)
    },
    userEdit: (params) => {
        return http.put('user', params)
    },
    userExcel: (params) => {
        return http.export('user/excel', params)
    }
}