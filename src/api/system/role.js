/**角色管理 */
import http from '../../utils/request';

export const role = {
    role: (params) => {
        return http.get('role', params)
    },
    roleAdd: (data) => {
        return http.post('role', data)
    },
    roleCheck: (params) => {
        return http.get(`role/check/${params}`)
    },
    roleMenu: (params) => {
        return http.get(`menu`, params)
    },
    roleMenuDetial: (params) => {
        return http.get(`role/menu/${params}`)
    },
    roleEdit: (params) => {
        return http.put(`role`, params)
    },
    roleDelete: (params) => {
        return http.delete(`role/${params}`)
    },
    roleExcel: (params) => {
        return http.export('role/excel', params)
    }
}