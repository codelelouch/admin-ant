/**菜单管理 */
import http from '../../utils/request';

export const dept = {
    dept: (params) => {
        return http.get('dept', params)
    },
    deptAdd: (data) => {
        return http.post('dept', data)
    },

    deptEdit: (params) => {
        return http.put(`dept`, params)
    },

    deptExcel: (params) => {
        return http.export('dept/excel', params)
    }
}