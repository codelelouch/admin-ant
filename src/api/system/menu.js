/** 菜单管理 */
import http from '../../utils/request';

export const menu = {
    menu: (params) => {
        return http.get('menu', params);
    },
    menuAdd: (data) => {
        return http.post('menu', data);
    },

    menuEdit: (params) => {
        return http.put('menu', params);
    },
    menuDelete: (params) => {
        return http.delete(`menu/${params}`);
    },
    menuExcel: (params) => {
        return http.export('menu/excel', params);
    }
};