/**菜单管理 */
import http from '../../utils/request';

export const dict = {
    dict: (params) => {
        return http.get('dict', params)
    },
    dictAdd: (data) => {
        return http.post('dict', data)
    },

    dictEdit: (params) => {
        return http.put(`dict`, params)
    },

    dictExcel: (params) => {
        return http.export('dict/excel', params)
    }
}