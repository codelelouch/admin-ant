import http from '../../utils/request';

export const notice = {
    change: (params) => {
        return http.get('/notice/change', params);
    },
    del: (data) => {
        return http.post('/notice/delete', data);
    },
    info: (params) => {
        return http.get('/notice/info', params);
    },
    list: (params) => {
        return http.get('/notice/page/list', params);
    },
    save: (data) => {
        return http.post('/notice/save', data);
    },
    matterList: () => {
        return http.get('/matter/list');
    }
};
