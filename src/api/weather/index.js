/**菜单管理 */
import http from '../../utils/request';

export const weather = {
    weather: (params) => {
        return http.get(`weather?areaId=${params}`)
    },
    deptAdd: (data) => {
        return http.post('dept', data)
    },

    deptEdit: (params) => {
        return http.put(`dept`, params)
    },

    deptExcel: (params) => {
        return http.export('dept/excel', params)
    }
}