import http from '../../utils/request';

export const recommend = {
    change: (params) => {
        return http.get('/recommend/change', params);
    },
    del: (data) => {
        return http.post('/recommend/delete', data);
    },
    info: (params) => {
        return http.get('/recommend/info', params);
    },
    list: (params) => {
        return http.get('/recommend/page/list', params);
    },
    save: (data) => {
        return http.post('/recommend/save', data);
    },
    matterList: () => {
        return http.get('/matter/list');
    }
};
