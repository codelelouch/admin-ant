export function triggerWindowResizeEvent () {
  let event = document.createEvent('HTMLEvents')
  event.initEvent('resize', true, true)
  event.eventType = 'message'
  window.dispatchEvent(event)
}

export default function getObjKey (target, keys) {
  var result = {}
  keys.forEach(key => {
    result[key] = target[key]
  })
  return result
}
/**
 *
 * @param {*} target
 * @param {*} keys
 * @returns
 */

export function setObJValue (target, keyObj) {
  var result = {}
  var arr = Object.keys(target)
  var arr1 = Object.keys(keyObj)
  arr.forEach(key => {
    var index = arr1.indexOf(key)
    if (index !== -1) {
      var key1 = arr1[index]
      if (!result[key]) { // 空值才给设置
        result[key] = keyObj[key1]
      }
    } else {
      result[key] = target[key]
    }
  })
  return result
}
