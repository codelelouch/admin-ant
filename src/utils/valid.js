export const validPhone = function(rule, value , callback){
    if(value == undefined || value == ''){
        callback()
    }else{
         const reg = /^[1][3,4,5,6,7,8,9][0-9]{9}$/
        if (!reg.test(value)) {
            callback('请输入正确手机号')
            return
        }
        callback()
    }
}