export const typeList = [
    {
        value: '1',
        label: '户政类',
        id: 1
    },
    {
        value: '2',
        label: '社保类',
        id: 2
    },
    {
        value: '3',
        label: '计生类',
        id: 3
    }, {
        value: '4',
        label: '市监类',
        id: 4
    }, {
        value: '5',
        label: '租赁类',
        id: 5
    }, {
        value: '6',
        label: '民政类',
        id: 6
    }, {
        value: '7',
        label: '其他',
        id: 7
    }, {
        value: '8',
        label: '全区通办类',
        id: 8
    }
];

