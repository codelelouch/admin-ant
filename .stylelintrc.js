module.exports = {
  'extends': 'stylelint-config-airbnb',
  'plugins': ['stylelint-order', 'stylelint-scss'],
  'rules': {
    "indentation": "tab",
    // scss 语法提示
    // 参考 https://github.com/stylelint/stylelint/issues/3190
    'at-rule-no-unknown': null,
    'scss/at-rule-no-unknown': true,
       "scss/dollar-variable-pattern": "$",
    'max-nesting-depth':10,
    'selector-max-id':10,

    // css书写顺序
    // 'order/order': [
    //   'declarations',
    //   'custom-properties',
    //   'dollar-variables',
    //   'rules',
    //   'at-rules'
    // ],
    // 'order/properties-order': [
    //   'position',
    //   'z-index',
    //    // 其他样式的顺序
    // ],
    // 其他规则
    'no-empty-source': null,
  }
}