
module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    commonjs: true,
    es6: true,
  },
  extends: ["plugin:vue/recommended", "eslint:recommended"],
  parserOptions: {
    // parser: "@babel/eslint-parser",

    parser: "babel-eslint",
    ecmaFeatures: {
      jsx: true,
    },
  },
  rules: {
    "vue/no-side-effects-in-computed-properties":0,
    "vue/html-indent": ["error", 4, {
      "attribute": 1,
      "baseIndent": 1,
      "closeBracket": 0,
      "alignAttributesVertically": true,
      "ignores": []
    }],
    "no-prototype-builtins":0,
      //"prettier/prettier": "error",
      //不希望在应用程序中使用的全局变量名称
     "no-restricted-globals": ["error", "event", "fdescribe"],
      //使用setter设置对象属性，必须同时使用getter。否则将无异于
      "accessor-pairs": 2,
      //数组中允许使用空格
      "array-bracket-spacing": 0,
      //变量在定义块的外部使用时，规则会生成警告。主要解决变量提升问题(忽略)
      "block-scoped-var": 0,
      //左大括号必须和标识符同一行，右括号在标识符同一行，且允许一个块打开和关闭括号在同一行上
      "brace-style": [2, "1tbs", { "allowSingleLine": true }],
      //不检测变量命名,开启则强制小驼峰命名
      "camelcase": 0,
      //数组,对象,imports,exports 不允许尾逗号
      "comma-dangle": [2, "never"],
      //声明多个变量时，需要在逗号补一个空格
      "comma-spacing": [2, { "before": false, "after": true }],
      //逗号必须在字面量后面
      "comma-style": [2, "last"],
      //分支数量检测,不进行限制
      "complexity": 0,
      //不检测计算的属性括号内的空格
      "computed-property-spacing": 0,
      //函数必须显式返回值,不可以返回undefined
      "consistent-return": 0,
      //是否打开this的指定别名控制,是,this 只能赋值到self命名的变量
      "consistent-this": [2,"self"],
      //派生类的构造函数必须调用super()。非派生类的构造函数不得调用super()
      "constructor-super": 2,
      //块只有一条语句可以省略花括号,但是必须标识符
      "curly": [2, "multi-line"],
      //switch 可以省略 default
      "default-case": 0,
      //对象的属性换行时，点必须跟随对象属性
      "dot-location": [2, "property"],
      //对象属性访问器，不禁用。
      "dot-notation": 0,
      //在非空文件中跟踪换行符,不进行添加
      "eol-last": 0,
      //比较运算符必须使用全等比较，除了比较null 和 undefine
      "eqeqeq": [0, "allow-null"],
      //检测for循环是否是一个停止条件永远无法到达的循环
      "for-direction": 2,
      //不禁止使用命名函数表达式
      "func-names": 0,
      //不限制函数定义的方式
      "func-style": 0,
      //生成器的星号必须写在函数名之前
      "generator-star-spacing": [2, { "before": true, "after": false }],
      //getter 定义的属性必须有return
      "getter-return": 2,
      //yield 星号必须写在表达式之前
      "yield-star-spacing": [2, { "before": true, "after": false }],
      //for in 必须使用 hasOwnProperty。不检测
      "guard-for-in": 0,
      //嵌套块和语句的特定缩进, 4个空格
      "indent": [1, 4, { "SwitchCase": 1 }],
      //不允许在对象名和冒号之间使用空格，允许在冒号和字面量之间使用空格
      "key-spacing": [2, { "beforeColon": false, "afterColon": true }],
      //是否强制执行统一的行结尾,否
      "linebreak-style": 0,
      //注释是否空行,否
      "lines-around-comment": 0,
      //嵌套回调是否做限制,否
      "max-nested-callbacks": 0,
      //允许在没有new操作符的情况下调用大写启动的函数:false,要求new使用大写启动函数调用所有操作符
      "new-cap": [2, { "newIsCap": true, "capIsNew": false }],
      //new关键字调用不带参数的构造函数时需要括号
      "new-parens": 2,
      //声明变量后，是否需要空行,否不需要
      "newline-after-var": 0,
      //允许使用alert
      "no-alert": 0,
      //不允许使用数组构造函数创建字面量的数组，可以根据长度创建
      "no-array-constructor": 0,
      //arguments.caller 并 arguments.callee
      "no-caller": 2,
      //catch 不检测形参变量是否在外部存在
      "no-catch-shadow": 0,
      //if语句检测赋值表达式
      "no-cond-assign": 2,
      //允许使用console
      "no-console": 0,
      //if for while do...while ?:不允许使用三元表达式
      "no-constant-condition": 0,
      //允许使用continue
      "no-continue": 0,
      //正则不允许使用控制字符
      "no-control-regex": 2,
      //不允许有断点
      "no-debugger": 2,
      //不允许删除变量
      "no-delete-var": 2,
      //要求使用正则表达式来避免分部操作器,否
      "no-div-regex": 0,
      //不允许在函数声明或表达式中使用重复的参数名称
      "no-dupe-args": 2,
      //不允许在对象文字中使用重复键
      "no-dupe-keys": 2,
      //不允许在switch语句的case子句中使用重复的测试表达式
      "no-duplicate-case": 2,
      //if包含return 后续的else是否需要检测return,否
      "no-else-return": 0,
      //允许空块语句
      "no-empty": 0,
      //不允许在正则表达式中使用空字符类
      "no-empty-character-class": 2,
      //允许直接判断null
      "no-eq-null": 0,
      //不允许使用eval
      "no-eval": 2,
      //不允许将另一个值分配给异常参数
      "no-ex-assign": 2,
      //不允许直接修改内建对象的原型
      "no-extend-native": 2,
      //bind必须正确的被使用到
      "no-extra-bind": 2,
      //禁止不必要的布尔转换
      "no-extra-boolean-cast": 2,
      //仅在必要时限制使用括号,否
      "no-extra-parens": 0,
      //禁止使用不必要的分号,否
      "no-extra-semi": 0,
      //switch穿透的问题
      "no-fallthrough": 2,
      //小数不允许缩写
      "no-floating-decimal": 2,
      //函数不能被重新分配
      "no-func-assign": 2,
      //定时器禁用evel
      "no-implied-eval": 2,
      //允许在代码行中进行注释
      "no-inline-comments": 0,
      //不允许在嵌套块中声明函数
      "no-inner-declarations": [2, "functions"],
      //不允许构造无意义的正则
      "no-invalid-regexp": 2,
      //不允许使用特殊的字符
      "no-irregular-whitespace": 2,
      //不允许使用迭代器接口
      "no-iterator": 2,
      //label不能和var同一个作用域声明
      "no-label-var": 2,
      //允许使用label
      "no-labels": 0,
      //不允许有多余的块结构
      "no-lone-blocks": 2,
      //不允许将if语句作为else块中的唯一语句,关闭
      "no-lonely-if": 0,
      //循环是否允许自调函数，允许
      "no-loop-func": 0,
      //所有需要声明必须是相同类型,否
      "no-mixed-requires": 0,
      //不允许使用混合空格和制表符进行缩进
      "no-mixed-spaces-and-tabs": 2,
      //禁止在逻辑表达式，条件表达式，声明，数组元素，对象属性，序列和函数参数周围使用多个空格
      "no-multi-spaces": 1,
      //使用斜线，可以在 JavaScript 中创建多行字符串
      "no-multi-str": 0,
      //不能超过一行空行
      "no-multiple-empty-lines": [1, { "max": 1 }],
      //不允许修改只读全局变量
      "no-native-reassign": 2,
      //规则不允许否定in表达式中的左操作数
      "no-negated-in-lhs": 2,
      //允许嵌套三元表达式
      "no-nested-ternary": 0,
      //使用new必须储存实例
      "no-new": 2,
      //允许使用new function
      "no-new-func": 0,
      //不允许new Object
      "no-new-object": 2,
      //不允许new require
      "no-new-require": 2,
      //不允许手动包装基础类型
      "no-new-wrappers": 2,
      //不允许直接调用Math JSON Feflect
      "no-obj-calls": 2,
      //不允许使用八进制数字文字
      "no-octal": 2,
      //不允许字符串文字中的八进制转义序列
      "no-octal-escape": 2,
      //函数参数允许被修改
      "no-param-reassign": 0,
      //node __dirname,__filename 允许直接使用字符串拼接
      "no-path-concat": 0,
      //process.env 允许在全局使用
      "no-process-env": 0,
      //允许 process.exit() 使用
      "no-process-exit": 0,
      //允许使用 __proto__
      "no-proto": 0,
      //不允许在同一个作用域同一个变量名声明多次
      "no-redeclare": 2,
      //正则表达式不允许有多个空格
      "no-regex-spaces": 2,
      //指定你不想在你的应用程序中使用的模块
      "no-restricted-modules": 0,
      //return 的表达式不允许被赋值，除非使用圆括号括起来
      "no-return-assign": 2,
      //允许location.href 使用 javascript:
      "no-script-url": 0,
      //条件语句，是否检测语句是否有判断意义，不检测
      "no-self-compare": 0,
      //不允许使用逗号运算符号
      "no-sequences": 2,
      //不消除阴影变量
      "no-shadow": 0,
      //不允许重新定义NaN，Infinity，undefined，eval，arguments
      "no-shadow-restricted-names": 2,
      //不允许功能标识符与其应用程序之间的间距
      "no-spaced-func": 2,
      //不允许稀疏数组
      "no-sparse-arrays": 2,
      //允许调用node的同步方法
      "no-sync": 0,
      //允许使用三元运算符号
      "no-ternary": 0,
      //使用this前必须调用super
      "no-this-before-super": 2,
      //抛出异常需要Error对象，利用静态分析限制可以跳过该检测
      "no-throw-literal": 2,
      //不允许在行尾添加尾随空白（空格，制表符和其他Unicode空白字符）
      "no-trailing-spaces": 1,
      //变量必须var定义
      "no-undef": 2,
      //定义变量不允许显式声明undefined
      "no-undef-init": 2,
      //undefined 可以被覆盖
      "no-undefined": 0,
      //允许在标识符中使用悬空下划线
      "no-underscore-dangle": 0,
      //表达式的结束必须使用分号，避免混淆
      "no-unexpected-multiline": 2,
      //不要使用三元运算符转布尔
      "no-unneeded-ternary": 2,
      //不允许可达代码前return，throw，continue，和break语句
      "no-unreachable": 2,
      //消除对程序无影响的代码片段,否
      "no-unused-expressions": 0,
      //警告未被使用的变量
      "no-unused-vars": [1],
      //使用变量必须先定义，放置变量提升,否
      "no-use-before-define": 0,
      //不允许使用var声明变量
      "no-var": 0,
      //允许使用void关键字的使用
      "no-void": 0,
      //注释不配置提示警告
      "no-warning-comments": 0,
      //不允许使用with
      "no-with": 2,
      //大括号内的空格不进行限制
      "object-curly-spacing": 0,
      //定义对象属性，兼容es5 es6
      "object-shorthand": 0,
      //每个作用域需要多个变量声明
      "one-var": [2, { "initialized": "never" }],
      //允许使用速记运算符
      "operator-assignment": 0,
      // 换行符号放在字面量/变量  前面，? ： 放在 字面量/变量 后面
      "operator-linebreak": [2, "after", { "overrides": { "?": "before", ":": "before" } }],
      //块内强制执行一致的空行填充，否
      "padded-blocks": 0,
      //变量如果为被修改，需要使用const声明,否:不进行限制
      "prefer-const": 0,
      //对象属性的key，定义方式不进行限制
      "quote-props": 0,
      //字符串使用单引号,只要字符串包含必须转义的引号，当字符串使用单引号或双引号时，此规则就不会报告问题
      "quotes": [2, "single", "avoid-escape"],
      //parentInt 必须强制提供基数
      "radix": 0,
       //语句结尾必须使用分号
      "semi": [2, "always"],
      //允许在分号前和分号后面添加空格
      "semi-spacing": 0,
      //检测变量是按字母顺序排序的
      "sort-vars": 0,
      //关键字之前至少一个空格
      "keyword-spacing": 2,
      //块的括号之前必须有空格
      "space-before-blocks": [2, "always"],
      //函数关键字和函数名之间需要空格,匿名函数不需要空格
      "space-before-function-paren": [2, "never"],
      //圆括号内侧禁止使用空格
      "space-in-parens": [2, "never"],
      //运算符前后必须要空格
      "space-infix-ops": 2,
      //一元运算符合操作符号前面或后面都需要一个空格
      "space-unary-ops": [2, { "words": true, "nonwords": false }],
      //注释行始必须包含空格，也允许以markers开头
      "spaced-comment": [2, "always", { "markers": ["global", "globals", "eslint", "eslint-disable", "*package", "!"] }],
      //不使用严格模式
      "strict": 0,
      //条件判断不允许判断NaN
      "use-isnan": 2,
      //是否验证jsdoc
      "valid-jsdoc": 0,
      //验证typeof 的比较字符串
      "valid-typeof": 2,
      //变量必须在作用域顶部声明,否
      "vars-on-top": 0,
      //立即调用的函数表达式都包含在圆括号中
      "wrap-iife": [2, "any"],
      //正则必须使用括弧包裹,否
      "wrap-regex": 0,
      //比较条件不能是yoda表达式
      "yoda": [2, "never"],

    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
  },
};
